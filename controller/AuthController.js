const { validationResult } = require("express-validator");
const { success, failure } = require("../util/common");
const UserModel = require("../model/User");
const AuthModel = require("../model/Auth");
const HTTP_STATUS = require("../constants/statusCodes");
const bcrypt = require("bcrypt");

class Auth {
    async create(req, res) {
        try {
            const validation = validationResult(req).array();
            if (validation.length > 0) {
                return res
                    .status(HTTP_STATUS.OK)
                    .send(failure("Failed to add the user", validation));
            }
            const { name, rank, email, address, role, password } = req.body;

            const emailCheck = await AuthModel.findOne({ email: email });
            if (emailCheck) {
                return res
                    .status(HTTP_STATUS.UNPROCESSABLE_ENTITY)
                    .send(failure("User with email already exists"));
            }
            let hashedPassword = await bcrypt
                .hash(password, 10)
                .then((hash) => {
                    return hash;
                })
                .catch((err) => {
                    return null;
                });
            const user = await UserModel.create({
                name: name,
                rank: rank,
                address: {
                    house: address.house,
                    road: address.road,
                    area: address.area,
                    city: address.city,
                    country: address.country,
                },
            });

            const auth = await AuthModel.create({
                email: email,
                password: hashedPassword,
                role: role,
                user: user._id,
            });
            if (user && auth) {
                return res.status(HTTP_STATUS.OK).send(
                    success("Successfully added the user", {
                        name: name,
                        email: email,
                    })
                );
            }
            return res
                .status(HTTP_STATUS.UNPROCESSABLE_ENTITY)
                .send(failure("Failed to add the user"));
        } catch (error) {
            console.log(error);
            return res
                .status(HTTP_STATUS.INTERNAL_SERVER_ERROR)
                .send(failure("Internal server error"));
        }
    }

    async login(req, res) {
        try {
            const validation = validationResult(req).array();
            if (validation.length > 0) {
                return res
                    .status(HTTP_STATUS.OK)
                    .send(failure("Please provide the email and password"));
            }

            const { email, password } = req.body;

            const auth = await AuthModel.findOne({ email: email }).populate("user");
            if (!auth) {
                return res.status(HTTP_STATUS.OK).send(failure("Email is not registered"));
            }

            const passwordCheck = await bcrypt.compare(password, auth.password);
            if (!passwordCheck) {
                return res.status(HTTP_STATUS.OK).send(failure("Email or password does not match"));
            }
            const response = auth.toObject();
            delete response.password;
            return res.status(HTTP_STATUS.OK).send(success("Successfully logged in", response));
        } catch (error) {
            console.log(error);
            return res
                .status(HTTP_STATUS.INTERNAL_SERVER_ERROR)
                .send(failure("Internal server error"));
        }
    }
}

module.exports = new Auth();
