const { body } = require("express-validator");

const userValidator = {};

const authValidator = {
    create: [
        body("email")
            .exists()
            .withMessage("Email was not provided")
            .bail()
            .notEmpty()
            .withMessage("Email cannot be empty")
            .bail()
            .isString()
            .withMessage("Email must be a string")
            .bail()
            .isEmail()
            .withMessage("Email format is incorrect"),
        body("password")
            .exists()
            .withMessage("Password was not provided")
            .bail()
            .isString()
            .withMessage("Password must be a string")
            .bail()
            .isStrongPassword({
                minLength: 8,
                minNumbers: 1,
                minLowercase: 1,
                minUppercase: 1,
                minSymbols: 1,
            })
            .withMessage(
                "Password must contain 8 characters, a small letter, a capital letter, a symbol and a number"
            ),
    ],
    login: [
        body("email")
            .exists()
            .withMessage("Email was not provided")
            .bail()
            .notEmpty()
            .withMessage("Email cannot be empty"),
        body("password")
            .exists()
            .withMessage("Password was not provided")
            .bail()
            .isString()
            .withMessage("Password must be a string"),
    ],
};

module.exports = { userValidator, authValidator };
