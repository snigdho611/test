const express = require("express");
const routes = express();
const AuthController = require("../controller/AuthController");
const { authValidator } = require("../middleware/validation");

// routes.get("/all", AuthController.getAll);
// routes.get("/detail/:id", AuthController.getOneById);
routes.post("/sign-up", authValidator.create, AuthController.create);
routes.post("/login", authValidator.login, AuthController.login);

module.exports = routes;
